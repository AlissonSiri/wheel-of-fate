﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WheelOfFate.Contracts;

namespace WheelOfFate.API.Client
{
    public class WheelOfFateApiClient: IDisposable
    {

        private HttpClient client = new HttpClient();

        public WheelOfFateApiClient()
        {
            var url = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            client.BaseAddress = new Uri(url);
            PrepareHeaders();
        }

        private void PrepareHeaders()
        {
            client.DefaultRequestHeaders.Authorization = null;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        
        public async Task<TokenResponse> GetToken(string userName, string password)
        {
            PrepareHeaders();
            var content = new StringContent(string.Format("username={0}&password={1}&grant_type=password", userName, password));
            var response = await client.PostAsync("Token", content);
            response.EnsureSuccessStatusCode();
            var token = await response.Content.ReadAsAsync<TokenResponse>();
            return token;
        }

        public async Task<ApplicationUser> GetUserByEmail(string userName, string token)
        {
            PrepareHeaders();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var url = string.Format("api/users/by-email/{0}", userName);
            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var user = await response.Content.ReadAsAsync<ApplicationUser>();
            return user;
        }

        public async Task<ApplicationUser> GetUserById(string id, string token)
        {
            PrepareHeaders();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var url = string.Format("api/users/{0}", id);
            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var user = await response.Content.ReadAsAsync<ApplicationUser>();
            return user;
        }

        public async Task<ApplicationUser> GetUserByName(string userName, string token)
        {
            PrepareHeaders();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var url = string.Format("api/users/by-name/{0}", userName);
            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var user = await response.Content.ReadAsAsync<ApplicationUser>();
            return user;
        }

        public async Task<IEnumerable<ShiftResponse>> GetShiftsByTeamFromDate(string teamId, int quantity, DateTime from, string token)
        {
            PrepareHeaders();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            var url = string.Format("api/shifts/by-team-from-date/{0}/{1}/{2}", teamId, quantity, from.ToString("yyyy-MM-dd"));
            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var shifts = await response.Content.ReadAsAsync<IEnumerable<ShiftResponse>>();
            return shifts;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                client.Dispose();
            }
        }

    }
}
