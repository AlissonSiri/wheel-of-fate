﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using WheelOfFate.Domain.Entities;

namespace WheelOfFate.API.App_Start
{

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class WheelOfFateUserManager : UserManager<User>
    {

        public void ConfigureTokenProvider(IDataProtectionProvider dataProtectionProvider)
        {
            if (dataProtectionProvider != null)
            {
                this.UserTokenProvider = new DataProtectorTokenProvider<User>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }

        public WheelOfFateUserManager(IUserStore<User> store) : base(store)
        {
            // Configure validation logic for usernames
            this.UserValidator = new UserValidator<User>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
            };

            // Configure validation logic for passwords
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            this.UserLockoutEnabledByDefault = true;
            this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            this.MaxFailedAccessAttemptsBeforeLockout = 5;

        }

        public async Task<User> FindByNameOrEmailAsync(string userNameOrEmail)
        {
            if (userNameOrEmail.Contains("@"))
                return await this.FindByEmailAsync(userNameOrEmail) ?? await this.FindByNameAsync(userNameOrEmail);
            return await this.FindByNameAsync(userNameOrEmail);
        }

    }

    public static class UserExtension
    {

        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this User u, WheelOfFateUserManager manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(u, authenticationType);
            return userIdentity;
        }
    }

}