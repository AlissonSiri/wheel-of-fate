﻿using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LightInject.Web;
using WheelOfFate.Infrastructure.Data.Context;
using WheelOfFate.API.FluentScheduler.Jobs;
using WheelOfFate.Domain.Repositories;
using WheelOfFate.Domain.Services.Contracts;
using WheelOfFate.Domain.Services;
using WheelOfFate.Infrastructure.Data.Repositories;
using WheelOfFate.API.Services;

namespace WheelOfFate.API.App_Start
{
    public class LightInjectConfig
    {

        public static ServiceContainer container;

        public static void Config(HttpConfiguration config)
        {
            container = new ServiceContainer();
            container.RegisterApiControllers();
            //register other services
            container.EnablePerWebRequestScope();
            container.EnableWebApi(config);

            container.Register<WheelOfFateContext, WheelOfFateContext>(new PerRequestLifeTime());

            container.Register<IEngineerRepository, EngineerRepository>(new PerRequestLifeTime());
            container.Register<IWheelOfFateService, WheelOfFateService>(new PerRequestLifeTime());
            container.Register<INotifierService, UserNotifierService>(new PerRequestLifeTime());

        }

    }
}