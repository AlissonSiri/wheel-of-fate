﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WheelOfFate.Domain.Services.Contracts;
using WheelOfFate.Infrastructure.Data.Context;
using System.Data.Entity;
using WheelOfFate.API.App_Start;
using WheelOfFate.Domain.Services;
using WheelOfFate.Infrastructure.Data.Repositories;
using WheelOfFate.API.Services;

namespace WheelOfFate.API.FluentScheduler.Jobs
{
    public class ChooseEngineersJob : GenericJob
    {

        protected async override Task ExecuteAsync()
        {

            using (var context = new WheelOfFateContext())
            {
                var service = new WheelOfFateService(new EngineerRepository(context), new UserNotifierService());

                var teams = await context.Teams
                    .Include("Shifts")
                    .ToArrayAsync();
                foreach (var t in teams)
                {
                    var lastShift = t.Shifts.Any() ? t.Shifts.Select(s => s.Date).Max() : DateTime.Today;
                    if (DateTime.Today.AddDays(20) < lastShift)
                        continue;
                    await service.ChooseEngineers(t, lastShift, 20);
                }
                await context.SaveChangesAsync();
            }

        }
    }
}