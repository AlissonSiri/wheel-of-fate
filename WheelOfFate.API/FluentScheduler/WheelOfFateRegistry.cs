﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WheelOfFate.API.FluentScheduler.Jobs;
using WheelOfFate.Domain.Services;
using WheelOfFate.Infrastructure.Data.Repositories;

namespace WheelOfFate.API.FluentScheduler
{
    public class WheelOfFateRegistry: Registry
    {

        public WheelOfFateRegistry()
        {
            this.Schedule<ChooseEngineersJob>().NonReentrant().ToRunNow().AndEvery(2).Hours();
        }

    }
}