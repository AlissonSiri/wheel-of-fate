﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WheelOfFate.Domain.Entities;
using WheelOfFate.Domain.Services.Contracts;

namespace WheelOfFate.API.Services
{
    public class UserNotifierService : INotifierService
    {

        public Task MaximumDailyShiftsNotBeingRespected(Engineer engineer, DateTime date, int numberOfShiftsAttempted)
        {
            // should send email? should send an In-App message?
            return Task.FromResult(0);
        }

        public Task MinimumRestDaysNotBeingRespected(Engineer engineer, DateTime lastShift, DateTime attemptedShift)
        {
            // should send email? should send an In-App message?
            return Task.FromResult(0);
        }

    }
}