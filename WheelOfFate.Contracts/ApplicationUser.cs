﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;

namespace WheelOfFate.Contracts
{
    public class ApplicationUser : IUser<string>
    {

        public string Id { get; set; }

        public string UserName { get; set; }

        public string Token { get; set; }

        public string Email { get; set; }
        
    }
}
