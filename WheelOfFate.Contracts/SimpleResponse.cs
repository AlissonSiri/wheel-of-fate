﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFate.Contracts
{
    public class SimpleResponse
    {

        public string ErrorMessage { get; set; }

        public bool Status { get; set; }

        public SimpleResponse()
        {
            this.Status = true;
        }

        public static SimpleResponse WithError(string message, params object[] args)
        {
            var r = new SimpleResponse();
            r.Status = string.IsNullOrWhiteSpace(message);
            r.ErrorMessage = string.Format(message, args);
            return r;
        }

        public static SimpleResponse FromException(Exception e)
        {
            return SimpleResponse.WithError(e.Message);
        }
    }
}
