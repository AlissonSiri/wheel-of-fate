﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFate.Contracts
{
    public class TokenResponse: SimpleResponse
    {

        public string access_token { get; set; }

    }
}
