﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WheelOfFate.Domain.Entities;

namespace WheelOfFate.Domain.Repositories
{
    public interface IEngineerRepository
    {
        Task<IEnumerable<Engineer>> GetAvailableEngineersWithLastShiftBeforeDate(Team team, DateTime start);
    }
}
