namespace WheelOfFate.Infrastructure.Data.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using RandomNameGeneratorLibrary;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WheelOfFate.Domain.Entities;
    using WheelOfFate.Infrastructure.Data.Context;

    public sealed class Configuration : DbMigrationsConfiguration<WheelOfFateContext>
    {

        private const int HOW_MANY_ENGINEERS = 10;

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(WheelOfFateContext context)
        {
            // Add random Engineers
            if (!context.Engineers.Any())
            {
                var team = new Team("IT")
                {
                    MaximumDailyShiftsPerEngineer = 1,
                    MinimumRestDaysAfterSupporting = 1,
                    MinimumShiftsPerWeek = 1,
                    ShiftsPerDay = 2,
                    SkipWeekends = true
                };

                var personGenerator = new PersonNameGenerator();
                for (int i = 0; i < HOW_MANY_ENGINEERS; i++)
                {
                    var name = personGenerator.GenerateRandomFirstAndLastName();
                    team.Engineers.Add(new Engineer(name));
                }

                context.Teams.Add(team);
            }


        }
    }
}
