﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using WheelOfFate.Contracts;

namespace WheelOfFate.Presentation.App_Start.IdentityConfig
{
    public class WheelOfFateSignInManager : SignInManager<ApplicationUser, string>
    {

        public WheelOfFateSignInManager(WheelOfFateUserManager userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager) { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((WheelOfFateUserManager)this.UserManager, this.AuthenticationType);
        }

        public async override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            try
            {
                var token = await WheelOfFateApplication.apiClient.GetToken(userName, password);
                var user = await WheelOfFateApplication.apiClient.GetUserByName(userName, token.access_token);
                user.Token = token.access_token;
                await SignInAsync(user, isPersistent, shouldLockout);
                return SignInStatus.Success;
            }
            catch (Exception e)
            {
                // should log?
                string error = e.Message;
            }
            return SignInStatus.Failure;
        }

        public static WheelOfFateSignInManager Create(IdentityFactoryOptions<WheelOfFateSignInManager> options, IOwinContext context)
        {
            return new WheelOfFateSignInManager(context.GetUserManager<WheelOfFateUserManager>(), context.Authentication);
        }
    }
}