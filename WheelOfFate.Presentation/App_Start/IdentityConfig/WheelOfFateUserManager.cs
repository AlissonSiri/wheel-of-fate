﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using WheelOfFate.Contracts;

namespace WheelOfFate.Presentation.App_Start.IdentityConfig
{

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class WheelOfFateUserManager : UserManager<ApplicationUser>
    {

        public void ConfigureTokenProvider(IDataProtectionProvider dataProtectionProvider)
        {
            if (dataProtectionProvider != null)
            {
                this.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }

        public WheelOfFateUserManager(IUserStore<ApplicationUser> store) : base(store)
        {

        }

        public async Task<ApplicationUser> FindByNameOrEmailAsync(string userNameOrEmail)
        {
            if (userNameOrEmail.Contains("@"))
                return await this.FindByEmailAsync(userNameOrEmail) ?? await this.FindByNameAsync(userNameOrEmail);
            return await this.FindByNameAsync(userNameOrEmail);
        }

    }

    public static class UserExtension
    {

        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this ApplicationUser u, WheelOfFateUserManager manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(u, authenticationType);
            userIdentity.AddClaim(new Claim("Token", u.Token));
            // Add custom user claims here
            return userIdentity;
        }
    }

}