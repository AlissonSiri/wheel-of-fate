﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using WheelOfFate.Presentation.App_Start.IdentityConfig;
using WheelOfFate.Presentation.Models;

namespace WheelOfFate.Presentation
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext<WheelOfFateUserManager>(Startup.CreateUserManager);
            app.CreatePerOwinContext<WheelOfFateSignInManager>(WheelOfFateSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

        }

        public static WheelOfFateUserManager CreateUserManager(IdentityFactoryOptions<WheelOfFateUserManager> options, IOwinContext context)
        {
            var manager = new WheelOfFateUserManager(new WheelOfFateStore());
            manager.ConfigureTokenProvider(options.DataProtectionProvider);
            return manager;
        }

    }
}