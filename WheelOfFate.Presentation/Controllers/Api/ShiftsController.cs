﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace WheelOfFate.Presentation.Controllers.Api
{
    [Authorize]
    [RoutePrefix("api/shifts")]
    public class ShiftsController : ApiController
    {

        [Route("upcoming/{teamId}", Name = "Api.Shifts.Upcoming")]
        public async Task<IHttpActionResult> GetUpcoming(string teamId)
        {
            var claims = User.Identity as ClaimsIdentity;
            var token = claims.Claims.Where(c => c.Type == "Token").First().Value;
            var shifts = await WheelOfFateApplication.apiClient.GetShiftsByTeamFromDate(teamId, 10, DateTime.Today, token);
            return Ok(shifts);
        }

    }
}