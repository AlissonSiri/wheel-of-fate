﻿
(function (window, document, $, undefined) {

    Date.prototype.yyyymmdd = function () {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
        ].join('');
    };

    // NOW TIMER
    // ----------------------------------- 
    $(function () {

        $('[data-now]').each(function () {
            var element = $(this),
                format = element.data('format');

            function updateTime() {
                var dt = moment(new Date()).format(format);
                element.text(dt);
            }

            updateTime();
            setInterval(updateTime, 1000);

        });
    });

    var grid = $("#grid-upcoming-shifts").bootgrid({
        ajax: false,
        searching: false,
        searchSettings: {
            delay: 250,
            characters: 3
        },
        formatters: {
            "date": function (column, row) {
                return moment(row.ShiftDate).format('YYYY/MM/DD');
            },
            "avatar": function (column, row) {
                return '<img alt="image" width="24" height="24" class="img-responsive img-circle" src="Content/images/unknown.gif">';
            }
        }
    });

    function UpdateCurrent(currentEngineer, nextEngineer) {
        $('#current-engineer-name').html(currentEngineer.EngineerName);
        $('#next-engineer-name').html(nextEngineer.EngineerName);
    }

    function LoadData() {
        var url = $('#urls').data('upcoming');
        $.get(url, function (data) {

            UpdateCurrent(data[0], data[1]);

            var upcoming = data.slice(2);

            grid.bootgrid('clear');
            grid.bootgrid('append', upcoming);
        });
    }

    LoadData();


})(window, document, window.jQuery);
